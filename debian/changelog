gerris (20131206+dfsg-21) unstable; urgency=medium

  * apply debdiff for 64-bit time_t (-19.1)

 -- Drew Parsons <dparsons@debian.org>  Sun, 24 Nov 2024 23:55:08 +0100

gerris (20131206+dfsg-20) unstable; urgency=medium

  * debian patches gcc-14.patch, gcc-14_2.patch fix build for gcc-14.
    Thanks Stephane Popinet. Closes: #1074988.
  * install all gerris modules (/usr/lib/<arch>/gerris/*)
    libtide uses netcdf. Closes: #1050781.
  * use dh_installman to install manpages (list usr/share/man/man1/*
    in gerris.manpages)
  * install static libraries in libgfs-dev
  * upstream test suite is written in python2, so don't run tests at
    build time

 -- Drew Parsons <dparsons@debian.org>  Sun, 24 Nov 2024 22:11:27 +0100

gerris (20131206+dfsg-19.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rename libraries for 64-bit time_t transition.  Closes: #1062102

 -- Steve Langasek <vorlon@debian.org>  Wed, 28 Feb 2024 17:00:58 +0000

gerris (20131206+dfsg-19) unstable; urgency=medium

  * Team upload.
  * Port to Python 3 (Closes: #943026).
  * Port to MPI 3.0.
  * Drop unneeded patch to use avconv rather than ffmpeg.
  * Update to debhelper-compat (= 12).
  * Update Standards-Version to 4.5.0 (no changes required).
  * Add Rules-Requries-Root: no.
  * Fix Vcs-* fields to point to salsa.

 -- Stuart Prescott <stuart@debian.org>  Mon, 10 Feb 2020 20:37:14 +1100

gerris (20131206+dfsg-18) unstable; urgency=medium

  * Team upload.
  * [7b124aa] Switch from libav-tools to ffmpeg. (Closes: #873192)
  * [8483d85] Set Standards-Version: 4.1.1
  * [0cd79ae] Remove --parallel option from d/rules because of compat leverl 10
  * [1ea3ef6] Set the maximal hardening options
  * [ca74e70] Remove myself from uploaders
  * [a2c7839] Remove testsuite field from d/control

 -- Anton Gladky <gladk@debian.org>  Thu, 23 Nov 2017 21:50:59 +0100

gerris (20131206+dfsg-17) unstable; urgency=medium

  * [55ab455] Depend gerris on gcc. (Closes: #862459)

 -- Anton Gladky <gladk@debian.org>  Sat, 13 May 2017 13:38:33 +0200

gerris (20131206+dfsg-16) unstable; urgency=medium

  * [719a48f] Add dh-exec as a build-depends. (Closes: #861690)

 -- Anton Gladky <gladk@debian.org>  Tue, 02 May 2017 21:43:03 +0200

gerris (20131206+dfsg-15) unstable; urgency=medium

  * [d9065f4] Respect arch-path for symbolic links. (Closes: #861677)

 -- Anton Gladky <gladk@debian.org>  Tue, 02 May 2017 19:06:33 +0200

gerris (20131206+dfsg-14) unstable; urgency=medium

  * [51edbdb] Fix brokin symlinks. (Closes: #861024)

 -- Anton Gladky <gladk@debian.org>  Mon, 24 Apr 2017 20:43:51 +0200

gerris (20131206+dfsg-13) unstable; urgency=medium

  [ Graham Inggs ]
  * [cfb12be] Allow stderr output in the autopkgtests, see #814451

 -- Anton Gladky <gladk@debian.org>  Tue, 27 Dec 2016 19:38:16 +0100

gerris (20131206+dfsg-12) unstable; urgency=medium

  [ Graham Inggs ]
  * Add Depends on mpi-default-bin to debian/tests/control. (Closes: #841885)

 -- Anton Gladky <gladk@debian.org>  Wed, 26 Oct 2016 21:37:43 +0200

gerris (20131206+dfsg-11) unstable; urgency=medium

  * [b87f1f2] Remove _rte_ in autopkgtest.

 -- Anton Gladky <gladk@debian.org>  Tue, 27 Sep 2016 20:37:13 +0200

gerris (20131206+dfsg-10) unstable; urgency=medium

  * [72790c8] Replace *orte_rsh* by *plm_rsh* in autopkgtest.
  * [415e7f2] Apply cme fix dpkg.

 -- Anton Gladky <gladk@debian.org>  Mon, 26 Sep 2016 20:35:28 +0200

gerris (20131206+dfsg-9) unstable; urgency=medium

  * [e21da76] Add libgfs-dev to control file of autopkgtest.
  * [aad908e] Set compat level 10.

 -- Anton Gladky <gladk@debian.org>  Wed, 15 Jun 2016 22:28:29 +0200

gerris (20131206+dfsg-8) unstable; urgency=medium

  * [07e7d30] Remove serial version of gerris. (Closes: #815825)
  * [0d89c5d] Apply cme fix dpkg.
  * [e94bd00] Simplify d/rules.
  * [9e9d5c0] Recommend gfsview.

 -- Anton Gladky <gladk@debian.org>  Sun, 22 May 2016 21:05:21 +0200

gerris (20131206+dfsg-7) unstable; urgency=medium

  * [43c738d] Fix compiler in build_function. (Closes: #815824)

 -- Anton Gladky <gladk@debian.org>  Thu, 25 Feb 2016 21:10:50 +0100

gerris (20131206+dfsg-6) unstable; urgency=medium

  [ Reiner Herrmann ]
  * [7af3877] Sort with C locale to get reproducible results. (Closes: #795868)

  [ Anton Gladky ]
  * [cfbf348] Apply cme fix dpkg-control.
  * [ed6802c] Fix syminks. (Closes: #773038)
  * [f46e087] Minor fix in patch description.
  * [bd2cfa4] Ignore quilt dir

 -- Anton Gladky <gladk@debian.org>  Mon, 17 Aug 2015 21:29:09 +0200

gerris (20131206+dfsg-5) unstable; urgency=medium

  * [dab307d] Add libgts-dev to Depends section of gerris. (Closes: #771291)

 -- Anton Gladky <gladk@debian.org>  Tue, 02 Dec 2014 22:38:58 +0100

gerris (20131206+dfsg-4) unstable; urgency=medium

  * [8a53e18] Remove testCons autopkgtest.

 -- Anton Gladky <gladk@debian.org>  Thu, 11 Sep 2014 20:44:13 +0200

gerris (20131206+dfsg-3) unstable; urgency=medium

  [ Lucas Kanashiro ]
  * [6418ba5] Fix autopkgtest. (Closes: #759529)

 -- Anton Gladky <gladk@debian.org>  Sat, 30 Aug 2014 22:27:30 +0200

gerris (20131206+dfsg-2) unstable; urgency=medium

  * [42fc4ea] Add libgfs-dev to Depends of gerris.
  * [4632bc4] Add missing dependencies in autopkgtests.
  * [99045e9] Recommend gfsview for gerris.

 -- Anton Gladky <gladk@debian.org>  Fri, 08 Aug 2014 20:46:14 +0200

gerris (20131206+dfsg-1) unstable; urgency=medium

  * [8fe0565] Update watch-file.
  * [bb1c3c7] Use DEP-5 for d/copyright.
  * [653f6dd] Imported Upstream version 20131206+dfsg
  * [4180f06] Refresh patches.
  * [652d9bc] Add missing build_functions file.
  * [2574e98] Replace ffmpeg command by avconv.
  * [3141ff8] Add missing Makefile.in.
  * [23b1801] Minor update in install-files, use autoreconf. (Closes: #727867)
  * [295e03c] Use wrap-and-sort.
  * [a34c986] Add autopkgtest.

 -- Anton Gladky <gladk@debian.org>  Fri, 01 Aug 2014 00:32:32 +0200

gerris (20110329-dfsg.2-4) unstable; urgency=medium

  * [520d60b] Set versioned build-depends for gts. Fixes FTBFS.

 -- Anton Gladky <gladk@debian.org>  Tue, 15 Apr 2014 21:12:49 +0200

gerris (20110329-dfsg.2-3) unstable; urgency=medium

  * [27923c9] Set compat-level 9.
  * [3dd3286] Fix FTBFS (update patch).
  * [3b01f20] Update paths due to multiarch-prefix.
  * [16e562e] Remove quilt from BD,
  * [10f1bd8] Update description.
  * [76842ad] Add myself to uploaders.
  * [2c9bc39] Bump Standards-Version 3.9.5. No changes.
  * [4c93f57] Set canonical VCS-fields.

 -- Anton Gladky <gladk@debian.org>  Mon, 14 Apr 2014 22:01:45 +0200

gerris (20110329-dfsg.2-2~experimental1) experimental; urgency=low

  * Test against mpi-defaults 1.0 in experimental
  * Standards-Version: 3.9.2

 -- Drew Parsons <dparsons@debian.org>  Tue, 14 Jun 2011 23:36:30 +1000

gerris (20110329-dfsg.2-1) unstable; urgency=low

  * New upstream snapshot 2011-03-29 (tarball 110330 + DSFG removal of
    RstarTree and fes2004 modules).
  * Standards-Version: 3.9.1.
  * Switch to dpkg-source 3.0 (quilt) format
    (enables upstream .bz2 tarball and other advantages)

 -- Drew Parsons <dparsons@debian.org>  Thu, 07 Apr 2011 12:16:54 +1000

gerris (20091109-dfsg.1-2) unstable; urgency=low

  * Use dh (debhelper v7) rather than cdbs to autobuild package.
    cdbs does not support multiple builds (mpi vs non-mpi).
  * Set up build-support for quilt (not actively used yet).
  * Remove get-orig-source target from debian/rules; gerris has
    nonfree code which needs to be removed from the upstream tarball
    anyway.
  * Introduction of new packages with MPI support. Currently conflicts
    with single processor builds (i.e. install one or the other, can't
    install simultaneously).
  * libgfs library package names bumped up to reflect increment in
    library soname.

 -- Drew Parsons <dparsons@debian.org>  Mon, 28 Dec 2009 14:20:42 +1100

gerris (20091109-dfsg.1-1) unstable; urgency=low

  * New upstream version.
  * Standards version 3.8.3.

 -- Drew Parsons <dparsons@debian.org>  Tue, 24 Nov 2009 23:08:04 +1100

gerris (20090512-dfsg.1-1) unstable; urgency=low

  * New upstream version.
    - upstream now releases snapshots only, rather than versioned
      releases, so upstream version is given by release date: 20090512.
    - modules/RStarTree has no free licence, so has been removed.
    - likewise modules/fes2004 removed: uses non-free Aktarus tide
      prediction functions.
    - fpucontrol-bug350595.patch superseded: src/init.c now uses
      fenv.h instead of fpu_control.h.  Closes: #525396.
  * debian/copyright lists licences for modules:
      - wavewatch: public domain
      	(see http://polar.ncep.noaa.gov/waves/wavewatch/wavewatch.html )
      - stokes: CW263.f confirmed by the author to be available in the
      	public domain.
  * With provision of the stokes module, /usr/lib/gerris now contains both
    arch dependent files (libstokes) and arch indep (m4.awk,
    gfs2tex.py), installed at /usr/share/gerris/lib.  /usr/lib/gerris
    is therefore no longer a symlink to /usr/share/gerris/lib, unlike
    earlier revisions.
  * fixed spelling in libgfs-dbg description. Closes: #527028.
  * Build-depends: gfortran (for stokes module).

 -- Drew Parsons <dparsons@debian.org>  Mon, 08 Jun 2009 00:54:21 +1000

gerris (0.9.2+darcs081022-dfsg.1-5) unstable; urgency=low

  * debian/control: Place libgfs-dbg in Section: debug.
  * debian/gerris.install:
      - install usr/share contents (MIME data, gerris icons).
      - install m4 and python scripts from usr/lib/gerris into
        usr/share/gerris/lib (arch-independent files, hence install in
        usr/share).  gerris.links creates symlink from
        usr/share/gerris/lib back to usr/lib/gerris.
        gerris Depends: m4
        Closes: #526424.

 -- Drew Parsons <dparsons@debian.org>  Mon, 04 May 2009 11:44:12 +1000

gerris (0.9.2+darcs081022-dfsg.1-4) unstable; urgency=low

  * Update fpucontrol-bug350595.patch:
    - #define FPU_AVAILABLE when FPU_SETCW and _FPU_IEEE are defined.
      Check FPU_AVAILABLE is defined at point where fpu_trap_exception
      is used. Closes: #523785.
    - Exclude armel (arm + softfp) from fpu manipulations. The
      instruction used in FPU_SETCW on arm is specific to the VFP unit
      which is not present on armel systems. Closes: #520878
  * debian/rules: disable mpi support with cdbs variable
      DEB_CONFIGURE_EXTRA_FLAGS = --disable-mpi
    to enable more consistent building on systems where mpi libs are
    installed.  In future we will build versions with and without mpi
    support.

 -- Drew Parsons <dparsons@debian.org>  Thu, 23 Apr 2009 15:44:23 +1000

gerris (0.9.2+darcs081022-dfsg.1-3) unstable; urgency=low

  [ Sylvestre Ledru ]
  * Check that _FPU_IEEE is actually available on the platform
    (Closes: #520866)

  [ Drew Parsons ]
  * Build-Depends: python.
    Some subdirs such as ./test use python at build time.

 -- Drew Parsons <dparsons@debian.org>  Wed, 25 Mar 2009 15:00:41 +1100

gerris (0.9.2+darcs081022-dfsg.1-2) unstable; urgency=low

  * Tighten dependency of libgfs-dev on libgfs-1.3-1 by specifying
    (= ${binary:Version}).
  * modules directory is removed from the source tarball for now due
    to licence concerns, so no need to actually mention those licences in
    the Debian copyright file.
  * Standards-Version: 3.8.1

 -- Drew Parsons <dparsons@debian.org>  Sun, 22 Mar 2009 22:56:01 +1100

gerris (0.9.2+darcs081022-dfsg.1-1) unstable; urgency=low

  [ Ruben Molina ]
  * New Maintainer (Closes: #471984)
  * New upstream version (Closes: #354032)
  * Acknowledge previous NMUs (Closes: #498305)
  * Migrate to CDBS
  * Update Standards to 3.8.0
  * Change section (math to science)
  * Update Depends (libnetcdf3 to libnetcdf4, libgsl0 to libgsl0ldbl)
  * Split libraries into a new binary packages
  * Add manpages
  * Repackage upstream sources removing modules/*
    (copyright/license issues). See debian/README.Debian-source
  * Add debian/get-orig-source.sh
  * Add debian/watch

  [ Drew Parsons ]
  * Set version to 0.9.2+darcs0810221-dfsg.1-1
  * The various libgfs shared libraries currently have the same
    soname version libgfs*-1.3.so.1, so put them in package
    libgfs-1.3-1.  Hopefully they will continue to keep the same
    soname in the future.
  * gerris uses its own header files when executing scripts (it
    compiles a binary at runtime for execution).
    Therefore gerris Depends: libgfs-dev.

 -- Drew Parsons <dparsons@debian.org>  Mon, 26 Jan 2009 01:44:28 +1100

gerris (0.6.0-3.2) unstable; urgency=low

   * Non-maintainer upload.
   * Add build-depends to libgts-bin. (Closes: #494243)
   * Fix typo in src/init.c to fix a FTBFS on m68k. (Closes: #350595)

 -- Holger Levsen <holger@debian.org>  Mon, 08 Sep 2008 11:11:34 +0000

gerris (0.6.0-3.1) unstable; urgency=low

  [ Brice Goglin ]
  * Non-maintainer upload.
  * Add a wrapper for fpu_control.h's difference on mips and m68k,
    closes: #350595.

  [ Christoph Berg ]
  * Rebuilding also fixes the libgts dependency, closes: #374023.

 -- Christoph Berg <myon@debian.org>  Fri, 12 Jan 2007 00:26:10 +0100

gerris (0.6.0-3) unstable; urgency=low

  * NMU
  * Apply various fixes for FTBFS on amd64. Thanks Andreas Jochens.
    (closes: #300446)

 -- David Nusinow <dnusinow@debian.org>  Mon, 16 Jan 2006 16:37:09 -0500

gerris (0.6.0-2) unstable; urgency=low

  * src/init.c: alpha doesn't have/need _FPU_SETCW (closes: bug#265823)

 -- Marcelo E. Magallon <mmagallo@debian.org>  Sat, 09 Apr 2005 10:48:53 -0600

gerris (0.6.0-1) unstable; urgency=low

  * New upstream release
  * Patched with upstream "patch1" patch.
  * debian/control: use glib 2.0, use gts 0.7.3

 -- Marcelo E. Magallon <mmagallo@debian.org>  Sat, 05 Feb 2005 14:14:23 -0600

gerris (0.2.0-1) unstable; urgency=low

  * New upstream release
  * Install shared libraries and header files in the gerris package.  If some
    other package wants to use these files, drop me a note and I'll split this
    out to their own packages.

 -- Marcelo E. Magallon <mmagallo@debian.org>  Sun, 09 Feb 2003 14:46:06 +0100

gerris (0.1.0-1) unstable; urgency=low

  * Initial Release. (closes: bug#150634)

 -- Marcelo E. Magallon <mmagallo@debian.org>  Sat, 29 Jun 2002 14:45:16 +0200
